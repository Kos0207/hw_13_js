let active;
let next;
let imgs = [];
let time = 3000;
let isPlay = true;

document.querySelectorAll(".image-to-show").forEach((img) => imgs.push(img));

function slideshow(imgs) {
  imgs.forEach((el, i) => {
    if (el.classList.contains("active")) {
      active = i;
      next = imgs.length - 1 === i ? 0 : i + 1;
    }
  });
  imgs[active].classList.remove("active");
  imgs[next].classList.add("active");
  // секундомір
  let count = 300;
  let counter = setInterval(timer, 10);
  
function timer() {
  if (count <= 0) {
    clearInterval(counter);
    return;
  }
  count--;
  document.querySelector(".time").innerHTML = count / 100 + " секунди.мілісекунди";
}
}

setInterval(() => {
  if (isPlay) {
    slideshow(imgs);
  }
}, time);

let btnStop = document.querySelector(".btn_stop");
let btnRestore = document.querySelector(".btn_restore");
let btnToggle = document.querySelector(".btn_toggle");

btnStop.addEventListener("click", function () {
  isPlay = false;
  console.log("paused");
});

btnRestore.addEventListener("click", function () {
  isPlay = true;
  console.log("play");
});
// універсальна кнопка
btnToggle.addEventListener("click", function () {
  isPlay = !isPlay;
  console.log("isPlay :", isPlay);
});
// поява кнопок
setTimeout(function () {
  document
    .querySelectorAll("button.hidden")
    .forEach((el) => el.classList.remove("hidden"));
}, 1500);
